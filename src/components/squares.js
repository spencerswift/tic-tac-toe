export const Square = (props) => {
  return (
    <button
      style={{
        height: '100%',
        width: '100%',
        borderRadius: 0,
        color: 'black',
        backgroundColor: 'white',
        border: '2px solid black',
        fontSize: '2.5em',
      }}
      onClick={props.onClick}
    >
      {props.value}
    </button>
  )
}
