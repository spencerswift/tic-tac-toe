import React from 'react'
import { Square } from './squares'

export class Board extends React.Component {
  renderSquare(i) {
    return (
      <Square
        value={this.props.squares[i]}
        onClick={() => this.props.onClick(i)}
      />
    )
  }

  render() {
    return (
      <div className='container '>
        <div className='row g-0 justify-content-center'>
          <div className='col-4 box'>{this.renderSquare(0)}</div>
          <div className='col-4 box'>{this.renderSquare(1)}</div>
          <div className='col-4 box'>{this.renderSquare(2)}</div>
        </div>

        <div className='row g-0 justify-content-center'>
          <div className='col-4 box'>{this.renderSquare(3)}</div>
          <div className='col-4 box'>{this.renderSquare(4)}</div>
          <div className='col-4 box'>{this.renderSquare(5)}</div>
        </div>
        <div className='row g-0 justify-content-center'>
          <div className='col-4 box'>{this.renderSquare(6)}</div>
          <div className='col-4 box'>{this.renderSquare(7)}</div>
          <div className='col-4 box'>{this.renderSquare(8)}</div>
        </div>
      </div>
    )
  }
}
