import React from 'react'
import { calculateWinner } from '../calculateWinner'
import { Board } from './board'

export class Game extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      history: [
        {
          squares: Array(9).fill(null),
        },
      ],
      stepNumber: 0,
      xIsNext: true,
    }
  }

  handleClick(i) {
    const history = this.state.history.slice(0, this.state.stepNumber + 1)
    const current = history[history.length - 1]
    const squares = current.squares.slice()

    if (calculateWinner(squares).winner || squares[i]) {
      return
    }
    squares[i] = this.state.xIsNext ? 'X' : 'O'
    this.setState({
      history: history.concat([
        {
          squares,
        },
      ]),
      stepNumber: history.length,
      xIsNext: !this.state.xIsNext,
    })
  }

  jumpTo(step) {
    this.setState({
      stepNumber: step,
      xIsNext: step % 2 === 0,
    })
  }

  render() {
    const history = this.state.history
    const current = history[this.state.stepNumber]
    const winInfo = calculateWinner(current.squares)
    const winner = winInfo.winner

    const moves = history.map((step, move) => {
      const desc = move ? 'Go to move #' + move : 'Go to game start'
      return (
        <li className='my-2' key={move}>
          <button className='btn-primary' onClick={() => this.jumpTo(move)}>
            {desc}
          </button>
        </li>
      )
    })

    let status

    if (winner) {
      status = 'Winner: ' + winner
      alert(`${winner} WON!`)
    } else {
      if (winInfo.isDraw) {
        alert("You can't even beat yourself?")
      } else {
        status = 'Next player: ' + (this.state.xIsNext ? 'X' : 'O')
      }
    }

    return (
      <div className='game'>
        <div className='container'>
          <header className='h1 text-center mt-5'>Tic-Tac-Toe</header>
          <header className='row justify-content-center'>
            <div
              className='col-lg-6 col-md-6 col-sm-12 game-board mt-5'
              align='center'
            >
              <Board
                squares={current.squares}
                onClick={(i) => this.handleClick(i)}
              />
            </div>
            <div
              className='col-lg-6 col-md-6 col-sm-12 game-info mt-5'
              align='center'
            >
              <div>{status}</div>
              <ol className='my-2' align='center'>
                {moves}
              </ol>
            </div>
          </header>
        </div>
      </div>
    )
  }
}
